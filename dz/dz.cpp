#include <windows.h>
#include <cmath>
#include "dz.h"

HINSTANCE hInst;
#define BUTTON_ID      1001
int LENY = 5000;
int START = -1000;
int FINISH = 1000;
int LENX = FINISH - START;

char szClassName[] = "askldfjasldfjakldjfalk", szWindowName[] = "dasdasd";

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
double Func(double);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow) {
	WNDCLASSEX wc;
	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = szClassName;
	wc.hIconSm = NULL;
	RegisterClassEx(&wc);
	HWND hwnd = CreateWindowEx(0, szClassName, szWindowName, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL);
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	hInst = hInstance;
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wp, LPARAM lp) {
	HDC hdc;
	HWND TextBox = 0;
	HWND TextBox2 = 0;
	PAINTSTRUCT ps;
	RECT rt;
	double x, y;
	switch (message) {
	case WM_CREATE: {
		HWND static_label = CreateWindow("Static", "-x:", WS_CHILD | WS_VISIBLE, 50, 500, 25, 20, hwnd, NULL, NULL, NULL);
		HWND TextBox = CreateWindow("EDIT", "", WS_BORDER | WS_CHILD | WS_VISIBLE, 70, 500, 50, 20, hwnd, NULL, NULL, NULL);
		HWND static_label2 = CreateWindow("Static", "x:", WS_CHILD | WS_VISIBLE, 185, 500, 25, 20, hwnd, NULL, NULL, NULL);
		HWND TextBox2 = CreateWindow("EDIT", "", WS_BORDER | WS_CHILD | WS_VISIBLE, 200, 500, 50, 20, hwnd, NULL, NULL, NULL);
		HWND hButton = CreateWindowW(L"button", L"Set", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 300, 500, 50, 20, hwnd, (HMENU)BUTTON_ID, hInst, NULL);

		return 0;
	}
	case WM_COMMAND:
	{
		if (LOWORD(wp) == BUTTON_ID) {
			TCHAR val[6] = { 0 }, val2[6] = { 0 };
			UpdateWindow(hwnd);
			//MessageBox(hwnd, "123", "123", MB_ICONINFORMATION);
			//SendMessage(TextBox, WM_GETTEXT, (WPARAM)6, (LPARAM)val);
			//SendMessage(TextBox2, WM_GETTEXT, (WPARAM)6, (LPARAM)val2);
			GetWindowText(TextBox, val, 6);
			GetWindowText(TextBox2, val2, 6);
			START = atoi(val);
			FINISH = atoi(val2);
			LENX = FINISH - START;
			UpdateWindow(hwnd);
			InvalidateRect(hwnd, NULL, TRUE);
		}
		return 0;
	}
	case WM_PAINT: {
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rt);
		SetAnisotropic(hdc, rt.right, rt.bottom, LENX, LENY);
		x = START;
		y = Func(x);
		MoveToEx(hdc, x, y, NULL);
		for (x = START + 1; x <= FINISH; ++x) {
			y = Func(x);
			if (y != -2000) {
				LineTo(hdc, x, y);
				MoveToEx(hdc, x, y, NULL);
			}
		}
		EndPaint(hwnd, &ps);
		return 0;
	}
	case WM_DESTROY: {
		PostQuitMessage(0);
		return 0;
	}
	default: {
		return DefWindowProc(hwnd, message, wp, lp);
	}
	}
}

double Func(double x) {
	return (5*asin(10*tan(abs(pow(0.5*x, 2) / x)) / abs(200*sin(10000*abs(10*x)))) + pow(0.05*x, 2) - 3);
}