#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>

#define BUTTON_ID      1001
int a = 1000, b = 1000;

HWND hWnd;
DWORD   dwThreadIdArray[2];
HANDLE  hThreadArray[2];

static TCHAR szWindowClass[] = _T("win32app");

static TCHAR szTitle[] = _T("4");

HINSTANCE hInst;

DWORD WINAPI th1(LPVOID lpParam) {
	OPENFILENAME ofn;
	wchar_t file_name[100];
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFile = file_name;
	ofn.lpstrFile[0] = '/0';
	ofn.nMaxFile = 100;
	ofn.lpstrFilter = L"Txt\0*.txt\0";
	ofn.nFilterIndex = 1;
	GetOpenFileName(&ofn);
	return (0);
}

DWORD WINAPI th2(LPVOID p) {
	if (GetAsyncKeyState(VK_CONTROL) != 0) {
		a = 5;
		b = 600;
	}
	else {
		a = 5000;
		b = 6000;
	}
	RECT rect2;
	rect2.left = 0;
	rect2.top = 500;
	rect2.right = 100;
	rect2.bottom = 700;
	InvalidateRect(hWnd, &rect2, TRUE);
	return (0);
}

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	if (!RegisterClassEx(&wcex))
	{
		MessageBox(NULL,
			_T("Call to RegisterClassEx failed!"),
			_T("Win32 Guided Tour"),
			NULL);

		return 1;
	}

	hInst = hInstance; 


	hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		500, 700,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	if (!hWnd)
	{
		MessageBox(NULL,
			_T("Call to CreateWindow failed!"),
			_T("Win32 Guided Tour"),
			NULL);

		return 1;
	}

	ShowWindow(hWnd,
		nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	TCHAR greeting[] = _T("CTRL");
	static HWND hButton;

	if(GetAsyncKeyState(VK_CONTROL) != 0) {
		hThreadArray[1] = CreateThread(
			NULL,
			0u,
			th2,
			NULL,
			0,
			&dwThreadIdArray[1]);
	}

	switch (message)
	{
	case WM_RBUTTONDOWN:
		exit(EXIT_FAILURE);
		break;
	case WM_CREATE:

		hButton = CreateWindowW(L"button", L"Open",
			WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
			100, 200,
			50, 20,
			hWnd, (HMENU)BUTTON_ID,
			hInst, NULL);

		break;
	case WM_COMMAND:
		hThreadArray[0] = CreateThread(
			NULL,                  
			0u,                     
			th1,       
			NULL,         
			0,                      
			&dwThreadIdArray[0]);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);

		TextOut(hdc,
			a, b,
			greeting, _tcslen(greeting));

		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

	return 0;
}