#pragma once
#include <Windows.h>
#include "resource.h"
#define IDI_ICON1       101
#define IDI_ICON2       101

#define IDTB_TOOLBAR   1000

#define IDB_TOOLBITMAP 1001

#define TB_TEST1       1002

#define TB_TEST2       1003

class MainWindow
{
public:
	HWND hEditBox;
	enum RESOURCE {
		FILE_NEW = 1, FILE_OPEN, FILE_SAVE, FILE_SAVE_AS, FILE_EXIT,
		FILE_FONT
	};

	MainWindow(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow, LRESULT(*WindowProcedure)(HWND, UINT, WPARAM, LPARAM));
	~MainWindow();

	void start();
	void addComponents(HWND hWnd, HWND& hEditB);
	void newFile(HWND hWnd);
	void save(HWND hWnd);
	void getSaveFilePath(HWND hWnd);
	void getOpenFilePath(HWND hWnd);
	void exit(HWND hWnd);
	void font(HWND hWnd, HWND& hEditB);

private:
	WNDCLASSW wc = {};
	HINSTANCE hInstance, hPrevInstance;
	LPSTR lpCmdLine;
	bool saveChanged = false;
	char path[10000];
	int nCmdShow;
	HWND hMainWindow;
	LRESULT(*WindowProcedure)(HWND, UINT, WPARAM, LPARAM);

	void initWindow();
	void saveFile(const LPSTR& file);
	void openFile(const LPSTR& file);
	char* getTime();
	bool insertIntoEditControl(char* txt);
	void clearEditControl();
};
